#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <windows.h>
#include <istream>
#include <fstream>

#define _WIN32_WINNT 0x0500

using namespace std;

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;

class Object
{
	protected:
		string o_name;
		string o_description;

	public:
		Object(string name, string description);
		string getName();
		void setName(string name);
		string getDescription();
		void setDescription(string description);
		string print();
};

class Key : public Object
{
	public:
		Key(string name, string descriptions) : Object(name, descriptions) {};

		string getName();
		void setName(string name);
		string getDescription();
		void setDescription(string description);
		string print();
};

class Room
{
	protected:
		string r_name;
		string r_description;
		int r_posX;
		int r_posY;
		int r_posZ;

	public:
		vector<Object> objects;

		Room();
		Room(string name, string description, int x, int y, int z);
		string getName();
		void setName(string name);
		string getDescription();
		void setDescription(string description);
		int getPosX();
		void setPosX(int x);
		int getPosY();
		void setPosY(int y);
		int getPosZ();
		void setPosZ(int z);
		bool isEmpty();
};

int loadRoomData(vector< vector < vector<Room> > > &rooms);
int loadObject(vector< vector < vector<Room> > > &rooms);
string peekInventory(vector<Object> &inventory);
INT initializeEmptyRooms(vector< vector < vector<Room> > > &rooms, int MAX_LENGTH, int MAX_WIDTH, int MAX_HEIGHT);
int output(vector< vector < vector<Room> > > &rooms, int x, int y, int z, int current_progress, int max_progress);