#include "stdafx.h"
#include "CA2.h"

#pragma region
Object::Object(string name, string description)
{
	o_name = name;
	o_description = description;
}

string Object::getName()
{
	return o_name;
}

void Object::setName(string name)
{
	o_name = name;
}

string Object::getDescription()
{
	return o_description;
}

void Object::setDescription(string description)
{
	o_description = description;
}

string Object::print()
{
	return o_name + ": " + o_description;
}

string Key::getName()
{
	return Object::getName();
}

void Key::setName(string name)
{
	Object::setName(name);
}

	string Key::getDescription()
{
	return Object::getDescription();
}

void Key::setDescription(string description)
{
	Object::setDescription(description);
}
string Key::print()
{
	return Object::print();
}

Room::Room()
{
	r_name = "";
	r_description = "";
	r_posX = -1;
	r_posY = -1;
	r_posZ = -1;
	objects = vector<Object>();
}

Room::Room(string name, string description, int x, int y, int z)
{
	r_name = name;
	r_description = description;
	r_posX = x;
	r_posY = y;
	r_posZ = z;
}

string Room::getName()
{
	return r_name;
}
		
void Room::setName(string name)
{
	r_name = name;
}

string Room::getDescription()
{
	return r_description;
}

void Room::setDescription(string description)
{
	r_description = description;
}

int Room::getPosX()
{
	return r_posX;
}

void Room::setPosX(int x)
{
	r_posX = x;
}

int Room::getPosY()
{
	return r_posY;
}

void Room::setPosY(int y)
{
	r_posY = y;
}

int Room::getPosZ()
{
	return r_posZ;
}

void Room::setPosZ(int z)
{
	r_posZ = z;
}

bool Room::isEmpty()
{
	if (r_name == "")
	{
		return true;
	}
	return false;	
}


int takeObject(vector<Object> &inventory, Key obj)
{
	inventory.push_back(obj);

	return 0;
}

int loadObject(vector< vector < vector<Room> > > &rooms)
{
	rooms[0][0][1].objects.push_back(Key("Key", "This is a key"));
	rooms[3][1][1].objects.push_back(Key("Tools", "This is a box of repair tools"));

	return 0;
}

string peekInventory(vector<Object> &inventory)
{
	string mes = "Inventory: \n";

	if (inventory.empty())
	{
		mes = "There is nothing in your inventory!";
	}

	for (int i = 0; i < inventory.size(); i++)
	{
		mes += to_string(i + 1) + ". ";
		mes += inventory[i].getName();
	}

	return mes;
}

int initializeEmptyRooms(vector< vector < vector<Room> > > &rooms, int MAX_LENGTH, int MAX_WIDTH, int MAX_HEIGHT)
{
	/*
	Initializing 3 dimensional vector data for rooms
	source: http://www.cplusplus.com/forum/general/833/
	Room readable from vector like room[x][y][z] with coordinates
	*/
	for (int i = 0; i < MAX_HEIGHT; i++)
	{
		vector< vector<Room> > floor;

		for (int j = 0; j < MAX_WIDTH; j++)
		{
			vector<Room> row;

			for (int k = 0; k < MAX_LENGTH; k++)
			{
				row.push_back(Room()); // push to each column in the rows
			}

			floor.push_back(row); // push to each row on the floor
		}

		rooms.push_back(floor); // push for each floor
	}

	return 0;
}

int loadRoomData(vector< vector < vector<Room> > > &rooms)
{
	ifstream input("Room_Data.txt");

	string inputWord;

	int i = 0;

	if (!input)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
		cout << " Error reading the file / file doesn't exist" << endl;
		return -1;
	}

	do
	{
		bool validRoom = true;

		//---------------READ NAME--------------------------------

		string roomName = "";

		do
		{
			input >> inputWord;
			roomName += inputWord;

			if (inputWord[inputWord.size() - 1] != ';')
			{
				roomName += " ";
			}
		} while (inputWord[inputWord.size() - 1] != ';');

		roomName[roomName.size() - 1] = ' ';


		//---------------READ DESCRIPTION-----------------------------

		string roomDescription = "";

		do
		{
			input >> inputWord;
			roomDescription += inputWord;

			if (inputWord[inputWord.size() - 1] != ';')
			{
				roomDescription += " ";
			}
		} while (inputWord[inputWord.size() - 1] != ';');

		roomDescription[roomDescription.size() - 1] = ' ';

		//----------------READ X and Y COORDINATES------------------------

		int x = -1;

		input >> x;

		int y = -1;

		input >> y;

		int z = -1;

		input >> z;

		//--------------------------------------------------------

		if (validRoom)
		{
			rooms[x][y][z] = Room(roomName, roomDescription, x, y, z);

			i++;
		}

	} while (!input.eof());

	input.close();

	return 0;
}

int output(vector< vector < vector<Room> > > &rooms, int x, int y, int z, int current_progress, int max_progress)
{
	system("CLS"); //clear the console

				   //output room name and current progress
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10); //lightgreen
	cout.width(33); cout << std::left << rooms[x][y][z].getName();
	cout.width(33); cout << std::right << "Progress: " << current_progress << "/" << max_progress << endl;

	//output current position in coordinates
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
	cout << "\n" << "Current Position: " << x << "," << y << "," << z << endl;

	//output description of the room
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	cout << "\n" << rooms[x][y][z].getDescription() << endl;

	if (!rooms[x][y][z].objects.empty())
	{
		cout << "\nObject(s) in this room: ";
		for (int i = 0; i < rooms[x][y][z].objects.size(); i++)
		{
			cout << rooms[x][y][z].objects[i].getName() << endl;
		}
	}

	return 0;
}
#pragma endregion Implementations

int main()
{
	HWND console = GetConsoleWindow();
	MoveWindow(console, 100, 100, 800, 500, TRUE);

	//source: http://www.cplusplus.com/forum/beginner/5830/
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	/*
		Colors used:
		Green for Inputs
		Red for Error Messages
		White for Data Output
		Purple for Selected Action
		Light Blue for Data Input
	*/

	const int MAX_LENGTH = 10;
	const int MAX_WIDTH = 10;
	const int MAX_HEIGHT = 10;

	//current position
	int x = 0;
	int y = 0;
	int z = 2; //floor number

	int current_progress = 0;
	int max_progress = 6;

	vector< vector< vector<Room> > > rooms;
	vector<Object> inventory;
	
	initializeEmptyRooms(rooms, MAX_LENGTH, MAX_WIDTH, MAX_HEIGHT);
	loadRoomData(rooms);
	loadObject(rooms);

	output(rooms, x, y, z, current_progress, max_progress);

	bool exit = false;
	string message = "";
	string errorMessage = "";

	bool electricPower = true;
	bool engineRunning = true;
	bool engineBroken = true;
	bool backupBatteryRunning = false;
	bool engineFixed = false;

	while (!exit)
	{
		string action;

		#pragma region
		if (errorMessage == "")
		{
			output(rooms, x, y, z, current_progress, max_progress);

			if (message != "")
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
				cout << "\n" << message << endl;
			}

			cout << "\n->";
		}
		else
		{
			output(rooms, x, y, z, current_progress, max_progress);

			if (message != "")
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
				cout << "\n" << message << endl;
			}

			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
			cout << "\n" << errorMessage << endl;

			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
			cout << "\n->";
		}

		message = "";
		errorMessage = "";

		#pragma endregion Error Message Checks

		cin >> action;

		#pragma region
		if (action == "help")
		{
			message += "Commands:";
			message += "\n'north, south, east, west, up, down' to nagivate around.";
			message += "\n'take' to take item(s) in the room";
			message += "\n'usekey' use the key";
			message += "\n'fix' use the tools to fix something";
		}
		if(action == "north")
		{
			if (y == 0)
			{
				errorMessage = "You can't go further north!";
			}
			else if (rooms[x][y - 1][z].isEmpty())
			{
				errorMessage = "You can't go further north!";
			}
			else
			{
				y -= 1;
				errorMessage = "";
			}
		}
		else if (action == "east")
		{
			if (x == 9)
			{
			errorMessage = "You can't go further east!";
			}
			else if (rooms[x + 1][y][z].isEmpty())
			{
				errorMessage = "You can't go further east!";
			}
			else
			{
				x += 1;
			}
		}
		else if (action == "south")
		{
			if (y == 9)
			{
				errorMessage = "You can't go further south!";
			}
			else if (rooms[x][y + 1][z].isEmpty())
			{
				errorMessage = "You can't go further south!";
			}
			else
			{
				y += 1;
			}
		}
		else if (action == "west")
		{
			if (x == 0)
			{
				errorMessage = "You can't go further west!";
			}
			else if (rooms[x - 1][y][z].isEmpty())
			{
				errorMessage = "You can't go further west!";
			}
			else
			{
				x -= 1;
			}
		}
		else if (action == "up")
		{
			if (z == 9)
			{
				errorMessage = "You can't go further up!";
			}
			else if (rooms[x][y][z + 1].isEmpty())
			{
				errorMessage = "You can't go further up!";
			}
			else
			{
				z += 1;
			}
		}
		else if (action == "down")
		{
			if (z == 0)
			{
				errorMessage = "You can't go further down!";
			}
			else if (rooms[x][y][z - 1].isEmpty())
			{
				errorMessage = "You can't go further down!";
			}
			else
			{
				z -= 1;
			}
		}
		else if (action == "take")
		{
			if (rooms[x][y][z].objects.empty())
			{
				errorMessage = "There is no object in this room!";
			}
			else
			{
				for (int i = 0; i < rooms[x][y][z].objects.size(); i++)
				{
					inventory.push_back(rooms[x][y][z].objects[i]);

					if (rooms[x][y][z].objects[i].getName() == "Key")
					{
						current_progress += 1;
					}

					if (rooms[x][y][z].objects[i].getName() == "Tools")
					{
						current_progress += 1;
					}
				}
				rooms[x][y][z].objects.erase(rooms[x][y][z].objects.begin(), rooms[x][y][z].objects.end());
				message = "Object taken!";
			}
		}
		else if (action == "i" || action == "inventory")
		{
			message = peekInventory(inventory);
		}
		else if (action == "usekey")
		{
			bool hasKey = false;
			for (int i = 0; i < inventory.size(); i++)
			{
				if (inventory[i].getName() == "Key")
				{
					hasKey = true;
				}
			}

			if (rooms[x][y][z].getName() == "Main Engine Room " && hasKey && engineRunning)
			{
				message = "You shut down the engine! There's no power in the submarine now!";
				rooms[x][y][z].setDescription("The room is quiet now, but it is dark");
				current_progress += 1;
				engineRunning = false;
				electricPower = false;
			}
			else if(rooms[x][y][z].getName() == "Electrical System Room " && hasKey)
			{
				if (!electricPower)
				{
					message = "You activated the backup battery! The lights come on again";
					current_progress += 1;
					backupBatteryRunning = true;
				}
				else if(engineFixed && backupBatteryRunning)
				{
					cout << "\nYou turned off the battery, you fixed everything!" << endl;
					current_progress += 1;
				}
				else if (!engineRunning && backupBatteryRunning)
				{
					message = "Why turning off the battery when engine isn't running?";
				}
				else
				{
					message = "Why you want to turn on the battery?";
				}
			}
			else if(rooms[x][y][z].getName() != "Main Engine Room " && hasKey == true)
			{
				message = "There's nothing to use the key on.";
			}
			else
			{
				errorMessage = "You don't have the key!";
			}
		}
		else if (action == "fix")
		{
			bool hasTools = false;
			for (int i = 0; i < inventory.size(); i++)
			{
				if (inventory[i].getName() == "Tools")
				{
					hasTools = true;
				}
			}

			if (rooms[x][y][z].getName() == "Main Engine Room " && engineRunning && engineBroken)
			{
				exit = true;
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
				cout << "\nYou tried to fix the engine while it's running, you get electric shock!" << endl;
				cout << "GAME OVER!" << endl;
				cout << "" << endl;
			}
			else if (rooms[x][y][z].getName() == "Main Engine Room " && !engineRunning && !backupBatteryRunning && engineBroken)
			{
				message = "You cant fix the engine now, its too dark.";
			}
			else if (rooms[x][y][z].getName() == "Main Engine Room " && !engineRunning && backupBatteryRunning && engineBroken)
			{
				message = "You fixed the engine! The engine is back to its tip-top condition.";
				current_progress += 1;
				engineFixed = true;
				engineBroken = false;
			}
			else if (hasTools == true)
			{
				message = "There's nothing to use the tools on.";
			}
			else
			{
				message = "You don't have the tools!";
			}

		}
		else
		{
			errorMessage = "There no such command!";
		}
		#pragma endregion Input Checks		

		action = "";
		//clearing the stream of cin up to 10000 char
		cin.clear();
		cin.ignore(10000, '\n');

		if (current_progress == max_progress)
		{
			exit = true;
			cout << "" << endl;
			cout << "You passed the level!" << endl;
		}
	}

	return 0;
}
