Boarding Deck; This is the boarding deck; 0 0 2
Communication Room; Satellite Communication is located here; 1 0 2
Command Deck (Bridge); This is the command room; 0 0 1
Crew Rest Area; This is the crew rest / public area; 1 0 1
Dining Room; This is the dining room; 2 0 1
Kitchen; This is the kitchen; 3 0 1
Storage Room; This is the storage room; 3 1 1
Electrical System Room; This is the electrical system bay; 0 0 0
Torpedo / Weapon Room; Tons of weapon are here!; 1 0 0
Main Engine Room; This room is... NOISY, something is breaking; 2 0 0